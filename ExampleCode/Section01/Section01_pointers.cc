/*****************************************
 * Filename: Section01_pointers.cc 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 * 
 * This file demonstrates the purpose of * and &
 * and shows the results of several pointer 
 * operations 
 * **************************************/

#include<iostream>

/************************************
 * Function name: main
 * Preconditions: int, char **
 * Postconditions: int 
 * 
 * This is the main driver function
 * *********************************/
int main(int argc, char**argv)
{
	int x, y; // Integer values 
	int *p1, *p2; // Integer pointers 

    // Define the values in the registers
	x = -100; y = 200;
	
	// Set p1 and p2 equal to the addresses (&) of x and y 
	p1 = &x;  p2 = &y;    

    // Output the values of x and y, -100 and 200
	std::cout << x << " " << y << std::endl;
	
	// Output the addresses of x and y registers
	std::cout << &x << " " << &y << std::endl;
	
	// Output the value stores in p1 and p2, which contain
	// the addresses of the registers that contain x and y 
	std::cout << p1 << " " << p2 << std::endl;
	
	// Output the addresses of p1 and p1 registers
	std::cout << &p1 << " " << &p2 << std::endl;
	
	// Outputs the value contained at the address of p1 and p2
	// The addresses contained in p1 and p2 are x and y 
	// Therefore, will output the values of x and y, -100 and 200
	std::cout << *p1 << " " << *p2 << std::endl << std::endl;
	
	// Sets the value in the address pointed to be p1 equal to 300
	// Therefore, this sets x equal to 300
	*p1 = 300;
	
	// Sets the address pointed to by p1 equal to the address pointed
	// to be p2. Therefore, the registers for p1 and p2 contain the 
	// address of the register holding y.
	p1 = p2;
	
	// Output the values of x and y, 300 and 200
	std::cout << x << " " << y << std::endl;
	
	// Output the value stores in p1 and p2, which contain
	// the addresses of the registers. Both point to y 
	std::cout << p1 << " " << p2 << std::endl;
	
	// Output the addresses of p1 and p1 registers. Remain unchanged
	std::cout << &p1 << " " << &p2 << std::endl;
	
	// Outputs the value contained at the address of p1 and p2
	// The addresses contained in p1 and p2 are both y 
	// Therefore, will output the values of y and y, 200 and 200	
	std::cout << *p1 << " " << *p2 << std::endl << std::endl;

	// Sets the value in the address pointed to be p1 equal to 300
	// Therefore, this sets y equal to -100	
	*p1 = -100;
	
	// Set p2 equal to the address (&) of  x
	p2 = &x;
	
	// Output the values of x and y, 300 and -100
	std::cout << x << " " << y << std::endl;
	
	// Output the value stores in p1 and p2, which contain
	// the addresses of the registers. Both point to y 
	std::cout << p1 << " " << p2 << std::endl;
	
	// Outputs the value contained at the address of p1 and p2
	// Therefore, will output the values of y and x, -100 and 300
	std::cout << *p1 << " " << *p2 << std::endl << std::endl;

	return 0;
}
